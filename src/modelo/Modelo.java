/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public interface Modelo {

    public void create(Alumno alumno); // Crea un alumno nuevo

    public void update(Alumno alumno); // Actuzaliza el alumno.

    public void delete(Alumno alumno);  // Borrar el alunno con el id dado

    public Alumno read(String id);  // Obtiene un alumno

}
