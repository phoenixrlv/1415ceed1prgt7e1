/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import controlador.Controlador;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import modelo.Alumno;

/**
 *
 * @author paco
 */
public class VistaPantalla extends JFrame implements Vista {

    private JPanel jpDatos;
    private JPanel jpDatos1;
    private JPanel jpCrud;
    private JPanel jpMain;

    private JLabel jlId;
    private JTextField jtfId;
    private JLabel jlNombre;
    private JTextField jtfNombre;
    private JLabel jlEdad;
    private JTextField jtfEdad;
    private JLabel jlEmail;
    private JTextField jtfEmail;

    private JLabel jlNombre1;
    private JTextField jtfNombre1;
    private JLabel jlEdad1;
    private JTextField jtfEdad1;
    private JLabel jlEmail1;
    private JTextField jtfEmail1;

    private JButton jbCreate;
    private JButton jbRead;
    private JButton jbUpdate;
    private JButton jbDelete;
    private JButton jbExit;

    public VistaPantalla() {

        super("Alumno");

        JPanelDatos();
        JPanelDatos1();
        JPanelCrud();
        JPanelMain();

        setSize(1500, 1200);
        setLocationRelativeTo(null);
        setVisible(true);
        pack();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Cierra Aplicacion

    }

    // Definimos JPanel Datos
    private void JPanelDatos() {
        // Definimos JLabel y JTextField  de Datos
        jlId = new JLabel();
        jlId.setText("Id");
        jtfId = new JTextField();
        jtfId.setColumns(10);

        jlNombre = new JLabel();
        jlNombre.setText("Nombre");
        jtfNombre = new JTextField();
        jtfNombre.setColumns(10);

        jlEdad = new JLabel();
        jlEdad.setText("Edad");

        jtfEdad = new JTextField();
        jtfEdad.setColumns(10);

        jlEmail = new JLabel();
        jlEmail.setText("Email");

        jtfEmail = new JTextField();
        jtfEmail.setColumns(10);

        jtfId.setEnabled(false);
        jtfNombre.setEnabled(false);
        jtfEdad.setEnabled(false);
        jtfEmail.setEnabled(false);

        // Definimos JPanel de datos.
        Dimension d1 = new Dimension(500, 200);
        jpDatos = new JPanel();
        jpDatos.setLayout(new GridLayout(4, 0));
        jpDatos.setPreferredSize(d1);
        //jpDatos.setBackground(Color.CYAN);

        // Añadimos botones al JPanel de datos.
        add(jpDatos);
        jpDatos.add(jlId);
        jpDatos.add(jtfId);
        jpDatos.add(jlNombre);
        jpDatos.add(jtfNombre);
        jpDatos.add(jlEdad);
        jpDatos.add(jtfEdad);
        jpDatos.add(jlEmail);
        jpDatos.add(jtfEmail);

    }

    // Definimos JPanel Datos
    private void JPanelDatos1() {

        jlNombre1 = new JLabel();
        jlNombre1.setText("Nombre");
        jtfNombre1 = new JTextField();
        jtfNombre1.setColumns(10);

        jlEdad1 = new JLabel();
        jlEdad1.setText("Edad");

        jtfEdad1 = new JTextField();
        jtfEdad1.setColumns(10);

        jlEmail1 = new JLabel();
        jlEmail1.setText("Email");

        jtfEmail1 = new JTextField();
        jtfEmail1.setColumns(10);

        // Definimos JPanel de datos.
        Dimension d1 = new Dimension(500, 200);
        jpDatos1 = new JPanel();
        jpDatos1.setLayout(new GridLayout(4, 0));
        jpDatos1.setPreferredSize(d1);
        //jpDatos.setBackground(Color.CYAN);

        // Añadimos botones al JPanel de datos.
        add(jpDatos1);
        jpDatos1.add(jlNombre1);
        jpDatos1.add(jtfNombre1);
        jpDatos1.add(jlEdad1);
        jpDatos1.add(jtfEdad1);
        jpDatos1.add(jlEmail1);
        jpDatos1.add(jtfEmail1);

    }

    private void JPanelCrud() {

        // Definimos JPanel Crud.
        Dimension d4 = new Dimension(100, 50);
        jbCreate = new JButton();
        jbCreate.setPreferredSize(d4);

        jbRead = new JButton();
        jbRead.setPreferredSize(d4);

        jbUpdate = new JButton();
        jbUpdate.setPreferredSize(d4);

        jbDelete = new JButton();
        jbDelete.setPreferredSize(d4);

        jbExit = new JButton();
        jbExit.setPreferredSize(d4);

        jbCreate.setText("Create");
        jbRead.setText("Read");
        jbUpdate.setText("Update");
        jbDelete.setText("Delete");
        jbExit.setText("Exit");

        // Panel de CRUD
        Dimension d2 = new Dimension(500, 50);
        jpCrud = new JPanel();
        jpCrud.setLayout(new GridLayout(0, 5));
        jpCrud.setPreferredSize(d2);
        jpCrud.setBackground(Color.RED);

        jpCrud.add(jbCreate);
        jpCrud.add(jbRead);
        jpCrud.add(jbUpdate);
        jpCrud.add(jbDelete);
        jpCrud.add(jbExit);

        jbCreate.setActionCommand(CREATE);
        jbRead.setActionCommand(READ);
        jbUpdate.setActionCommand(UPDATE);
        jbDelete.setActionCommand(DELETE);
        jbExit.setActionCommand(EXIT);

    }

    // Definimos JPanel Main.
    private void JPanelMain() {
        Dimension d3 = new Dimension(1200, 300);
        jpMain = new JPanel();
        jpMain.setToolTipText("Main");
        jpMain.setPreferredSize(d3);
        jpMain.setBackground(Color.ORANGE);

        add(jpMain);
        jpMain.add(jpDatos);
        jpMain.add(jpDatos1);
        jpMain.add(jpCrud);
    }

    public void setControlador(Controlador c) {
        jbCreate.addActionListener(c);
        jbRead.addActionListener(c);
        jbUpdate.addActionListener(c);
        jbDelete.addActionListener(c);
        jbExit.addActionListener(c);
    }

    @Override
    public void arranca() {
        pack();// coloca los componentes
        setLocationRelativeTo(null);// centra la ventana en la pantalla
        setVisible(true);// visualiza la ventana
    }

    @Override
    public void exit() {
        System.exit(0);
    }

    @Override
    public void error(String error) {
        JOptionPane.showMessageDialog(null, error, "Error", 1);
    }

    public Alumno pedirAlumno() {

        Alumno alumno = null;

        String nombre = jtfNombre1.getText();
        String edad_ = jtfEdad1.getText();
        int edad = Integer.parseInt(edad_);
        String email = jtfEmail1.getText();
        alumno = new Alumno("", nombre, edad, email);
        return alumno;

    }

    public void mostrarAlumno(Alumno alumno) {

        jtfId.setText(alumno.getId());
        jtfNombre.setText(alumno.getNombre());
        int edad_ = alumno.getEdad();
        String edad = edad_ + "";
        jtfEdad.setText(edad);
        jtfEmail.setText(alumno.getEmail());

    }

    public String pedirId() {

        String id = JOptionPane.showInputDialog(null, "Id: ", "Alumno", 1);
        return id;
    }

}
