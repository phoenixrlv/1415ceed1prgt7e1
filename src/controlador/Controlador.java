package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import modelo.Alumno;
import modelo.Modelo;
import vista.Vista;

/**
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public class Controlador implements ActionListener {

    private Modelo modelo;
    private Vista vista;

    Controlador(Modelo modelo_, Vista vista_) throws IOException {
        modelo = modelo_;
        vista = vista_;
        vista.setControlador(this);
        vista.arranca();
    }

    public void actionPerformed(ActionEvent evento) {

        Alumno alumno = null;
        String id = null;
        String comando = evento.getActionCommand();

        switch (comando) {
            case Vista.EXIT: // Exit
                vista.exit();
                break;

            case Vista.READ:
                id = vista.pedirId();      // Pide id
                alumno = modelo.read(id);  // Obtiene alumno
                if (alumno == null) {
                    vista.error("Alumno con id " + id + " no se ha encontrado");
                } else {
                    vista.mostrarAlumno(alumno);   // Muestra alumno
                }
                break;

            case Vista.CREATE:
                alumno = vista.pedirAlumno();   // Obtiene el alumno
                modelo.create(alumno);     // Lo graba en el modelo
                vista.mostrarAlumno(alumno);
                break;

            case Vista.UPDATE:
                id = vista.pedirId();
                alumno = vista.pedirAlumno();
                alumno.setId(id);
                modelo.update(alumno);
                vista.mostrarAlumno(alumno);
                break;
            case Vista.DELETE: // Read
                id = vista.pedirId();
                alumno = new Alumno(id, "", 0, "");
                modelo.delete(alumno);
                break;
            default:
                vista.error("Operación no defina");
                break;
        }

    }

}
